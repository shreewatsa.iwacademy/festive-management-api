

var app = new Vue({
    delimiters: ['[[', ']]'],
    el: '#app',
    data: {
        message: 'Hello Vue!',
        users : []
    },
    methods: {
        greet: function (name) {
            console.log('Hello from ' + name + '!')
        },
        FetchData: function () {
            axios.get('http://127.0.0.1:8000/api/v1/user/').then(response => {
                console.log(response.data.results);
                this.users = response.data.results;
            });
        }
    },
    mounted() {
        this.FetchData();
    },
});