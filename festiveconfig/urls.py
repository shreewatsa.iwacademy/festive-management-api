"""festiveconfig URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import RedirectView

from rest_framework.authtoken.views import obtain_auth_token

# Schema and documentation

API_TITLE = "Festive APIs"
API_DESCRIPTION = "API endpoints for Festive Management Project"
from rest_framework.documentation import include_docs_urls
from rest_framework_swagger.views import get_swagger_view
swagger_schema_view = get_swagger_view(title=API_TITLE)

# end of schema and documentation

urlpatterns = [
    path('favicon.ico',RedirectView.as_view(url='/static/images/favicon.ico')),
    path('admin/', admin.site.urls),

    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', obtain_auth_token),

    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),

    path('', include('accounts.urls', namespace='accounts')),
    path('', include('services.urls', namespace='services')),
    path('', include('pages.urls', namespace='pages')),
    
    

    # Documenting API
    path('docs/', include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)), # Human friendly documentation of apis.
    path('swagger-docs/', swagger_schema_view),  

]
if settings.DEBUG: 
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
# rest-auth/ ^password/reset/$ [name='rest_password_reset']
# rest-auth/ ^password/reset/confirm/$ [name='rest_password_reset_confirm']
# rest-auth/ ^login/$ [name='rest_login']
# rest-auth/ ^logout/$ [name='rest_logout']
# rest-auth/ ^user/$ [name='rest_user_details']
# rest-auth/ ^password/change/$ [name='rest_password_change']
# rest-auth/registration/