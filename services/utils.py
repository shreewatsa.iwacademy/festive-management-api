from rest_framework import filters
import django_filters

from .models import Service

class ServiceDynamicSearchFilter(filters.SearchFilter):
    def get_search_fields(self, view, request):
        search_fields = request.GET.getlist('search_fields',[])
        breakpoint()
        val = search_fields if search_fields else ['title', 'description', 'seller__username']
        print("Search fields are : ",val)
        return val

class ServiceFilter(django_filters.FilterSet):
    # seller__username = django_filters.CharFilter(lookup_expr="icontains")
    seller = django_filters.CharFilter(field_name="seller__username", lookup_expr='icontains')
    min_price = django_filters.NumberFilter(field_name="rate_amount", lookup_expr='gte')
    max_price = django_filters.NumberFilter(field_name="rate_amount", lookup_expr='lte')
    class Meta:
        model = Service
        fields = ['category', "seller", "min_price", "min_price"]
        # fields = ['category',"seller__username", "seller"]

