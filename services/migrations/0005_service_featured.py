# Generated by Django 3.0.5 on 2020-04-02 11:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0004_servicecomments'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='featured',
            field=models.BooleanField(default=False),
        ),
    ]
