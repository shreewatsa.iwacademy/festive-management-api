# Generated by Django 3.0.5 on 2020-04-03 07:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('services', '0007_auto_20200402_1439'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='servicerating',
            unique_together=set(),
        ),
    ]
