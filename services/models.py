from django.db import models
from django.contrib.auth import get_user_model
# Create your models here.


class Category(models.Model):
    name = models.CharField(
        "service category", max_length=50, unique=True)

    class Meta:
        verbose_name_plural = "categoires"

    def __str__(self):
        return self.name


class Service(models.Model):
    seller = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    category = models.ForeignKey(
        Category, related_name="services", on_delete=models.SET_NULL, blank=True, null=True)
    title = models.CharField(max_length=255)
    description = models.TextField()
    rate_amount = models.DecimalField(max_digits=10, decimal_places=2)
    average_rating = models.DecimalField(
        max_digits=2, decimal_places=1, default=0)
    featured = models.BooleanField(default=False)

    def __str__(self):
        return f'Services by {self.seller.username} - {self.title}'

    # class Meta:
    #     ordering = ["average_rating", "title"]


class ServiceRating(models.Model):
    rate_by_user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    service = models.ForeignKey(Service, related_name="ratings", on_delete=models.CASCADE)
    rating = models.IntegerField()
    feedback = models.TextField()


class ServicePackage(models.Model):
    ECONOMY = 1
    STANDARD = 2
    PREMIUM = 3

    CHOICES = (
        (ECONOMY, 'Economy'),
        (STANDARD, 'Standard'),
        (PREMIUM, 'premium'),
    )

    service = models.ForeignKey(
        Service, related_name="packages", on_delete=models.CASCADE)
    details = models.TextField("Info about this particular package")
    package = models.PositiveIntegerField(choices=CHOICES, default=STANDARD)
    rate = models.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        unique_together = [('service', 'package')]


class ServiceComment(models.Model):
    service = models.ForeignKey(Service, related_name="comments", on_delete=models.CASCADE)
    comment_by = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    comment = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)