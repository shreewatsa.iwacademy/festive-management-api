from rest_framework import serializers

from . import models
from accounts.serializers import UserSerializer


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = "__all__"


class CommentSerializer(serializers.ModelSerializer):
    comment_by = UserSerializer()

    class Meta:
        model = models.ServiceComment
        fields = "__all__"


class ServiceSerializer(serializers.ModelSerializer):
    featured = serializers.BooleanField(read_only=True)
    seller = serializers.ReadOnlyField(source="seller.id")
    class Meta:
        model = models.Service
        fields = "__all__"


class ServiceRatingSerializer(serializers.ModelSerializer):
    rate_by_user = UserSerializer(read_only=True)
    class Meta:
        model = models.ServiceRating
        fields = ['service','rating','feedback','rate_by_user']

    def __str__(self):
        return f'Rating#{self.rating}'


class ServicePackageSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ServicePackage
        fields = "__all__"


class ServiceRetrieveSerializer(serializers.ModelSerializer):
    seller = UserSerializer(read_only=True)
    category = CategorySerializer(read_only=True)
    comments = CommentSerializer(many=True, read_only=True)
    ratings = ServiceRatingSerializer(many=True, read_only=True)
    packages = ServicePackageSerializer(read_only=True, many=True)

    class Meta:
        model = models.Service
        fields = ["id", 'title',
                  'description', 'featured', "rate_amount", "comments", 'ratings', "packages", 'seller', 'category']
        extra_kwargs = {
            'featured': {'read_only': True}
        }
    # def to_representation(self, instance):
    #     response = super().to_representation(instance)
    #     response['comments'] = CommentSerializer(instance.comments.all().order_by('created'), many=True).data
    #     return response
