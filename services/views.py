from django.shortcuts import render

from rest_framework import viewsets, permissions, filters, status
from rest_framework.decorators import action
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend

from . import models, utils
from . import serializers as service_serializers
from config import permissions as custom_permissions

from config.logging_config import logging
logger = logging.getLogger(__name__)

class ServiceModelView(viewsets.ModelViewSet):

    queryset = models.Service.objects.all()

    filter_backends = [utils.ServiceDynamicSearchFilter, DjangoFilterBackend, filters.OrderingFilter, ]
    
    ordering_fields = ['average_rating', 'rate_amount' ]
    ordering = ['-average_rating']  # default ordering
    
    # filterset_fields = ['category', 'rate_amount', 'seller__username']
    filterset_class = utils.ServiceFilter

    # search_fields = ['title', 'description', 'seller__username']
    # filter_backends = [filters.SearchFilter]
    
    @action(detail=True, methods=['get', 'post'], permission_classes=[permissions.IsAuthenticated])
    def rating(self, request, pk=None):
        service = self.get_object()
        breakpoint()
        if request.method == "POST":
            breakpoint()
            serializer = service_serializers.ServiceRatingSerializer(service=service, rate_by_user=request.user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                breakpoint()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(
                service_serializers.ServiceRatingSerializer(
                    service.ratings.all(), many=True).data,
                status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        breakpoint()
        serializer.save(seller=self.request.user)
    def create(self, request, *args, **kwargs):
        breakpoint()
        return super().create(request, *args, **kwargs)

    def get_permissions(self):
        permission_classes = []
        if self.action in ['list', 'retrieve']:
            permission_classes.append(permissions.AllowAny)
        elif self.action == "create":
            permission_classes.append(custom_permissions.IsSeller)
        else:
            permission_classes.append(custom_permissions.IsSellerOrReadOnly)
        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.action in ["list","create"]:
            return service_serializers.ServiceSerializer
        return service_serializers.ServiceRetrieveSerializer

    def get_queryset(self):
        # category = self.request.query_params.get('category',None)
        # if category:
        #     logger.debug(f"Queryset filtered by category : {category}")
        #     return super().get_queryset().filter(category=category).order_by('average_rating')
        # return super().get_queryset().order_by('average_rating')
        return super().get_queryset()



class CategoryModelViewSet(viewsets.ModelViewSet):
    queryset = models.Category.objects.all()
    serializer_class = service_serializers.CategorySerializer
    permission_classes = [custom_permissions.IsAdminOrReadOnly, ]

    def get_queryset(self):
        return super().get_queryset().order_by('id')
