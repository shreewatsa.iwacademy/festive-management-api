from django.urls import path,include

from . import views

from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register('service', views.ServiceModelView, basename="service")
router.register('category', views.CategoryModelViewSet, basename="category")

app_name = 'services'

urlpatterns = [
    path('api/v1/', include(router.urls)),
]


# URLS
# http://127.0.0.1:8000/api/v1/service/?search=bhoj&search_fields=featured&search_fields=description&category=3
# http://example.com/api/users?ordering=account,username
# http://example.com/api/users?ordering=-username
# http://127.0.0.1:8000/api/v1/service/?category=&ordering=rate_amount