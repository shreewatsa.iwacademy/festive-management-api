from django.core.validators import RegexValidator

phone_regex = RegexValidator(regex=r'^\+?\d{9,14}$', message="Phone number format: '+999999999'. Up to 14 digits allowed.")

