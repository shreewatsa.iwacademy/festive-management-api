from rest_framework import permissions

class IsSellerOrReadOnly(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.seller == request.user

class IsAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        return True if request.method in permissions.SAFE_METHODS else request.user.is_superuser

class IsSeller(permissions.BasePermission):
    def has_permission(self, request, view):
        # the user can be checked against group ie, seller group.
        return request.user.is_authenticated and request.user.is_seller