from django.shortcuts import render
from django.views import View
from django.contrib.auth import get_user_model

# Create your views here.

class HomePageView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'pages/index.html', {'user':request.user})
    def post(self, request, *args, **kwargs):
        pass