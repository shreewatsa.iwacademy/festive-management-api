from django.shortcuts import render
from django.contrib.auth import get_user_model
from rest_framework import viewsets, permissions, filters, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets

from .serializers import UserSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all().order_by('email')
    serializer_class = UserSerializer
    permission_classes  = []
    authentication_classes = []
