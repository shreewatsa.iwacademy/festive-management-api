from django.db import models
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth import authenticate
from django.contrib import auth
from django.utils import timezone
from django.core.mail import send_mail

from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model

from config.validators import phone_regex


class User(AbstractUser):
    email = models.EmailField(unique=True)
    avatar = models.ImageField(upload_to="avatars/", blank=True, null=True)
    nickname = models.CharField(max_length=50, null=True, blank=True)
    is_buyer = models.BooleanField(default=True)
    is_seller = models.BooleanField(default=False)
    is_mail_confirmed = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.username} - {self.email}'


class UserProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True, null=True)
    contact = models.CharField("contact number", validators=[
                               phone_regex], max_length=15)

    def __str__(self):
        if self.user:
            return f'Buyer#{id} - {user.username}'
        return f'Buyer#{id}'


class SellerProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    seller_details = models.TextField()
    logo = models.ImageField(upload_to="logo/", null=True, blank=True)
    banner = models.ImageField(upload_to='banners/', null=True, blank=True)
    established_in = models.DateField(null=True, blank=True)
    contact_primary = models.CharField("primary contact number",  validators=[
                                       phone_regex], max_length=15)
    contact_secondary = models.CharField("alternative contact number",  validators=[
                                         phone_regex], max_length=15, blank=True)
    is_featured = models.BooleanField(default=False)

    def __str__(self):
        return f'(Seller#{self.user.id}) - {self.user.username}'


# from django.contrib.gis.db import models
# class Location(models.Model):
#     user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, null=True, blank=True)
#     name = models.CharField(max_length=100)
#     location = models.PointField()
#     address = models.CharField(max_length=100)
#     city = models.CharField(max_length=50)
