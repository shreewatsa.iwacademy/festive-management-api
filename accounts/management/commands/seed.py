from django.core.management.base import BaseCommand, CommandError

from faker import Faker
fake = Faker()

from polls.models import Question as Poll


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('poll_ids', nargs='+', type=int)
        # Named (optional) arguments
        parser.add_argument(
            '--delete',
            action='store_true',
            help='Delete poll instead of closing it',
        )
        # # add user objects
        # parser.add_argument('--users',
        #     default=200,
        #     type=int,
        #     help='The number of fake users to create.'
        # )

    def handle(self, *args, **options):
        # if options['delete']:
            # poll.delete()
        for poll_id in options['poll_ids']:
            try:
                poll = Poll.objects.get(pk=poll_id)
            except Poll.DoesNotExist:
                raise CommandError('Poll "%s" does not exist' % poll_id)

            poll.opened = False
            poll.save()

            self.stdout.write(self.style.SUCCESS(
                'Successfully closed poll "%s"' % poll_id))

        # for _ in range(options['users']):
        #     UserFactory.create()


# $ python manage.py seed  or $ python manage.py seed --users 500
# $ python manage.py seed <poll_ids>