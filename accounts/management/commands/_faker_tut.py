import factory
from faker import Faker
from faker.providers import internet
from faker.providers import BaseProvider
from faker.generator import random
from datetime import datetime

my_word_list = [
    'danish', 'cheesecake', 'sugar',
    'Lollipop', 'wafer', 'Gummies',
    'sesame', 'Jelly', 'beans',
    'pie', 'bar', 'Ice', 'oat'
]

fake = Faker()
Faker.seed(4231)

fake.add_provider(internet)
print(fake.ipv4_private())

print()
print(fake.name())

print()
print(fake.bothify(letters='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                   text='+977-98########?'))

print()
print(fake.address())

print()
print(fake.text())


class Provider(BaseProvider):
    def category(self):
        return random.choice(['Sound System', 'Catering', 'Party Palace', 'Tent/Table/Chair', 'Fooding'])


fake.add_provider(Provider)
print(fake.category())


print(fake.sentence())
print(fake.sentence(ext_word_list=my_word_list))

# Standard list of providers
# faker.providers.address
# faker.providers.automotive
# faker.providers.bank
# faker.providers.barcode
# faker.providers.color
# faker.providers.company
# faker.providers.credit_card
# faker.providers.currency
# faker.providers.date_time
# faker.providers.file
# faker.providers.geo
# faker.providers.internet
# faker.providers.isbn
# faker.providers.job
# faker.providers.lorem
# faker.providers.misc
# faker.providers.person
# faker.providers.phone_number
# faker.providers.profile
# faker.providers.python
# faker.providers.ssn
# faker.providers.user_agent


class Book():
    def __init__(self, title, author_name, email):
        self.author_name = author_name
        self.title = title
        self.email = email
        self.published_at = None

    def __str__(self):
        return f'Book: {self.title}'


class BookFactory(factory.Factory):         # DjangoModelFactory : to use Django model.
    class Meta:
        model = Book

    title = factory.Faker('sentence', nb_words=4)
    author_name = factory.Faker('name')
    email = factory.LazyAttribute(lambda e: f'{e.author_name}@example.com')
    published_at = factory.LazyFunction(datetime.now())

b = BookFactory()
b2 = BookFactory(author_name = 'shreewatsa')
print(b2.author_name)
# same as BookFactory.create()
# Other options are : .build(), .stub(), .create() .

print(b)
print(b.author_name)


## Associations
# class PostFactory(factory.Factory):
#     class Meta:
#         model = 'post'

#     author = factory.SubFactory(UserFactory)
# post = PostFactory().build()


###   Using Factory Boy   ###


# class UserFactory(factory.django.DjangoModelFactory):
#     class Meta:
#         model = User

#     name = factory.Faker('name')
#     address = factory.Faker('address')
#     phone_number = factory.Faker('phone_number')

# $ python manage.py shell
# for _ in rage(100):
#     UserFactory.create()