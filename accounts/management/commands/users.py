from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand, CommandError

User = get_user_model()


class Command(BaseCommand):
    help = 'Test if custom command works or not.'

    def add_arguments(self, parser):
        parser.add_argument('user_ids', nargs='+', type=int)
        parser.add_argument(
            '--detail',
            action='store_true',
            help='display the details of the user',
        )

    def handle(self, *args, **options):

        # users   =  User.objects.all()
        # for user in users:
        #     self.stdout.write(self.style.SUCCESS(f'User#{user.id} | {user.username} | {user.email}'))
        #     # self.stdout.write(f'{user.__dict__}')
        for userid in options['user_ids']:
            try:
                user = User.objects.get(pk=userid)
                if options['detail']:
                    self.stdout.write(
                        f'User#{user.id} | {user.username} | {user.email}')
                else:
                    self.stdout.write(f'User#{user.id} | {user.username}') 
            except User.DoesNotExist:
                raise CommandError(f'User #{userid} does not exit !')

            # self.stdout.write(self.style.SUCCESS(f'User : {user.username}'))
