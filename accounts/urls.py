from django.urls import path, include

from . import views

from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register('user', views.UserViewSet, basename='user')

app_name = 'accounts'

urlpatterns = [
    # path('api/v1/', include(router.urls)), 
]
